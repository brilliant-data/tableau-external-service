# Tableau External Service PoC

## Description

PoC for using External Web Services with Tableau (Desktop and Server).

## Installation

Install requirements and start the tabpy server:

```bash
cd <repo_directory>
virtualenv .
source bin/activate
pip install -r requirements.txt
cd lib/python2.7/site-packages/tabpy_server
nohup ./startup.sh &
python convert_deploy.py
```

Press Ctrl-C to quit

## Configuration

### Desktop

1. Go to `Help`/`Setting and Performance`/`Manage External Service Connection...`
2. Use `tableau-external-service.brilliant-data.net` as Server and `9004` as Port 
3. Test Connection and push OK

### Server

Refer to http://onlinehelp.tableau.com/current/pro/desktop/en-us/help.html#r_connection_manage.html

```bash
tabadmin stop
tabadmin set vizqlserver.allow_insecure_scripts true
tabadmin set vizqlserver.script.disabled true
tabadmin set vizqlserver.extsvc.host tableau-external-service.brilliant-data.net
tabadmin set vizqlserver.extsvc.port 9004
tabadmin configure
tabadmin start
```




