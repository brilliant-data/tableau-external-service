import tabpy_client
from forex_python.converter import CurrencyRates

c = CurrencyRates()
client = tabpy_client.Client('http://localhost:9004/')

def convert_to(target,value):
  #print value
  lst= []
  rate= c.get_rate('USD',target[0])
  for i in range(0,len(value)) :
    lst.append(value[i]*rate)
  return lst
  #return convert_to(target,value)

client.deploy('convert_to',convert_to,'convert from USD', override = True)
